class Race 
	include Mongoid::Document
	field :name
	has_many :players
end
