class Role 
	include Mongoid::Document
	field :name
	field :category
	has_many :players
end
