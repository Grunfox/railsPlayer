class Player 
	include Mongoid::Document
	field :name
	field :level
	belongs_to :role
	belongs_to :race
end
