json.extract! player, :id, :name, :role, :level, :race, :created_at, :updated_at
json.url player_url(player, format: :json)