json.extract! role, :id, :name, :category, :created_at, :updated_at
json.url role_url(role, format: :json)